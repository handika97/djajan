import React, {useState, useEffect, useRef} from 'react';
import {
  View,
  Image,
  Text,
  TouchableOpacity,
  Animated,
  Keyboard,
  ScrollView,
  FlatList,
} from 'react-native';
import {createStackNavigator} from '@react-navigation/stack';
import {useSelector, useDispatch} from 'react-redux';
import {Login, Register} from '../screen/Auth';
import {
  Location,
  ListLocations,
  StoreList,
  CartList,
  DetailStore,
  CheckOut,
} from '../screen/Home';
import {createMaterialTopTabNavigator} from '@react-navigation/material-top-tabs';
import LinearGradient from 'react-native-linear-gradient';
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';
const Stack = createStackNavigator();
const Tab = createMaterialTopTabNavigator();

function AppNavigator() {
  const {isLogin} = useSelector((state) => state.auth);
  const [performShow, setPerformShow] = useState(true);
  const dispatch = useDispatch();
  const auth = useSelector((state) => state.auth);
  useEffect(() => {
    setTimeout(() => setPerformShow(false), 2000);
  });

  return (
    <Stack.Navigator headerMode="none">
      {/* {performShow && <Stack.Screen name="Splash" component={Splash} />} */}
      {isLogin ? (
        <Stack.Screen name="Home" component={Home} />
      ) : (
        <Stack.Screen name="Auth" component={Auth} />
      )}
    </Stack.Navigator>
  );
}

function Auth() {
  return (
    <Stack.Navigator headerMode="none">
      <Stack.Screen name="Login" component={Login} />
      <Stack.Screen name="Register" component={Register} />
    </Stack.Navigator>
  );
}

function Home() {
  return (
    <Stack.Navigator headerMode="none">
      <Stack.Screen name="Location" component={Location} />
      <Stack.Screen name="ListLocations" component={ListLocations} />
      <Stack.Screen name="StoreList" component={StoreList} />
      <Stack.Screen name="CartList" component={CartList} />
      <Stack.Screen name="store" component={store} />
      <Stack.Screen name="CheckOut" component={CheckOut} />
    </Stack.Navigator>
  );
}

function store({navigation}) {
  console.log(navigation);
  const [loading, setLoading] = useState(false);
  const TranslateY = useRef(new Animated.Value(550)).current;
  const cart = useSelector((state) => state.cart);
  useEffect(() => {
    if (loading) {
      Animated.timing(TranslateY, {
        toValue: 550,
        duration: 500,
        useNativeDriver: true,
      }).start();
    } else {
      Animated.timing(TranslateY, {
        toValue: 0,
        duration: 500,
        useNativeDriver: true,
      }).start();
    }
  }, [loading]);

  return (
    <LinearGradient
      start={{x: 0.0, y: 0.0}}
      end={{x: 1, y: 1.0}}
      locations={[0, 0.4, 0.7]}
      colors={['purple', '#db4dff', '#e580ff']}
      style={{
        flex: 1,
        paddingHorizontal: '3%',
        paddingTop: '5%',
        justifyContent: 'flex-end',
      }}>
      {cart.data.length > 0 ? (
        <TouchableOpacity
          style={{
            position: 'absolute',
            alignSelf: 'center',
            backgroundColor: 'purple',
            zIndex: 1,
            height: 40,
            bottom: 20,
            width: '70%',
            alignItems: 'center',
            borderRadius: 100,
            elevation: 2,
            justifyContent: 'center',
            padding: 5,
            flexDirection: 'row',
          }}
          onPress={() => navigation.navigate('Home', {screen: 'CheckOut'})}>
          <Text style={{color: 'white', fontSize: 20, marginHorizontal: 10}}>
            Rp. {`${cart.price}`}
          </Text>
          <FontAwesome5 name="shopping-cart" color="white" size={20} />
        </TouchableOpacity>
      ) : null}
      <Tab.Navigator
        style={{borderTopLeftRadius: 20, borderTopRightRadius: 20}}>
        <Tab.Screen name="Warung" component={Store} />
        <Tab.Screen name="Keranjang" component={CartList} />
      </Tab.Navigator>
    </LinearGradient>
  );
}

function Store() {
  return (
    <Stack.Navigator headerMode="none">
      <Stack.Screen name="Warung" component={StoreList} />
      <Stack.Screen name="DetailStore" component={DetailStore} />
    </Stack.Navigator>
  );
}

export default AppNavigator;
