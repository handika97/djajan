import {createSlice} from '@reduxjs/toolkit';
// import Geolocation from 'react-native-geolocation-service';

const authSlice = createSlice({
  name: 'auth',
  initialState: {
    data: null,
    error: null,
    isLogin: false,
    loading: false,
    headers: {
      Authorization: 'Basic ZGVrYXBlOkQzcjRwS3JlNHMhcGVyNERBQDIwMjA=',
      token: '',
      'Content-Type': 'application/json',
    },
  },
  reducers: {
    authLoginPending: (state) => {
      state.isLoading = true;
    },
    authLoginRejected: (state, action) => {
      state.isLoading = false;
      state.error = action.payload;
    },
    authLoginReceive: (state, action) => {
      // const data = action.payload.data;
      state.isLogin = true;
      // state.data = data;
      // state.headers.token = action.payload.data.token;
    },
    onErrorRead: (state) => {
      state.error = null;
    },
    authLogOut: (state) => {
      state.isLogin = false;
    },
  },
});

export const {
  authLoginPending,
  authLoginRejected,
  authLoginReceive,
  authLogOut,
} = authSlice.actions;

export default authSlice.reducer;
import {post} from './main';
const defaultBody = null;

export const authLogin = (email, password) => {
  let bodyData = {
    email: email,
    password: password,
  };
  return (dispatch) => {
    // dispatch(
    //   post(
    //     'http://192.168.137.1:5000/admin/login',
    //     bodyData,
    //     (res) => {
    //       alert('success');
    //       console.log(res);
    dispatch(authLoginReceive('res'));
    //     },
    //     (err) => {
    //       alert('err');
    //       console.log(err);
    //     },
    //   ),
    // );
  };
};

export const authRegister = (name, email, password) => {
  let bodyData = {
    name: name,
    email: email,
    password: password,
  };
  console.log(bodyData);
  return (dispatch) => {
    dispatch(
      post(
        'http://192.168.137.1:5000/admin/register',
        bodyData,
        (res) => {
          alert('success');
          console.log(res);
        },
        (err) => {
          alert('err');
          console.log(err);
        },
      ),
    );
  };
};

export const onErrorReset = () => {
  return (dispatch) => {
    dispatch(onErrorRead());
  };
};
