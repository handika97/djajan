import {createSlice} from '@reduxjs/toolkit';
import Axios from 'axios';
// import reactotron from 'reactotron-react-native';
export const slice = createSlice({
  name: 'main',
  initialState: {},
  reducers: {},
});

export const {} = slice.actions;

export const post = (
  link,
  data,
  // ApiClient,
  ifSuccess = () => {},
  ifError = () => {},
  finallyDo = () => {},
) => (dispatch, getState) => {
  const {auth} = getState();
  Axios.post(link, data, {
    headers: auth.headers,
  })
    .then((res) => {
      ifSuccess(res);
      console.log(res);
    })
    .catch((err) => {
      ifError(err);
      console.log(err);
    })
    .finally(() => {
      finallyDo();
    });
};

export default slice.reducer;
