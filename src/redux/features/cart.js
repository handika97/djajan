import {createSlice} from '@reduxjs/toolkit';
// import Geolocation from 'react-native-geolocation-service';
import {useDispatch, useSelector} from 'react-redux';

const cartSlice = createSlice({
  name: 'cart',
  initialState: {
    data: [],
    price: 0,
    store: 0,
  },
  reducers: {
    Data: (state, action) => {
      state.data = state.data.concat(action.payload[0]);
      state.price += parseInt(action.payload[0].item.Price);
    },
    Add: (state, action) => {
      state.data[
        state.data
          .map((x) => {
            return x.id;
          })
          .indexOf(action.payload.id)
      ].qty += 1;
      state.price += parseInt(action.payload.Price);
    },
    Min: (state, action) => {
      state.data[
        state.data
          .map((x) => {
            return x.id;
          })
          .indexOf(action.payload.id)
      ].qty -= 1;
      state.data = state.data.filter((x) => {
        return x.qty > 0;
      });
      state.price -= parseInt(action.payload.Price);
    },
    Reset: (state) => {
      state.data = [];
      state.price = 0;
    },
    AddStore: (state, action) => {
      console.log('redux', action.payload);

      state.store = action.payload;
    },
  },
});

export const {Data, Add, Min, Reset, AddStore} = cartSlice.actions;

export default cartSlice.reducer;

const defaultBody = null;

export const Plus = (item, cart) => {
  return (dispatch) => {
    console.log(item);
    let newArr = [...cart.data];
    if (newArr.length > 0) {
      if (
        newArr
          .map(function (obj) {
            return obj.id;
          })
          .indexOf(item.id) === -1
      ) {
        dispatch(Data([{item: item, qty: 1, id: item.id}]));
      } else {
        for (let i = 0; i < newArr.length; i++) {
          if (item.id === newArr[i].id) {
            dispatch(Add(item));
          }
        }
      }
    } else {
      dispatch(Data([{item: item, qty: 1, id: item.id}]));
    }
  };
};

export const Minus = (item, cart) => {
  return (dispatch) => {
    let newArr = [...cart.data];
    for (let i = 0; i < newArr.length; i++) {
      if (item.id === newArr[i].id) {
        dispatch(Min(item));
      }
    }
  };
};

export const Empty = (id) => (dispatch, getState) => {
  dispatch(Reset());
  dispatch(AddStore(id));
};
