import React, {useState, useEffect} from 'react';
import {View, Text, Image, TouchableOpacity, TextInput} from 'react-native';

const Button = ({title, onPress = () => {}, small}) => {
  return (
    <TouchableOpacity
      style={{
        height: small ? '100%' : 40,
        backgroundColor: 'purple',
        top: small ? 0 : 30,
        borderRadius: 10,
        elevation: 2,
        alignItems: 'center',
        justifyContent: 'center',
      }}
      onPress={() => onPress()}>
      <Text style={{color: 'white', fontSize: small ? 13 : 20}}>{title}</Text>
    </TouchableOpacity>
  );
};

export default Button;
