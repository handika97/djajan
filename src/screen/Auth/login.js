import React, {useState, useEffect, useRef} from 'react';
import {
  View,
  Image,
  Text,
  TouchableOpacity,
  Animated,
  Keyboard,
} from 'react-native';
import {Input, Button} from '../../component/cell';
import {useDispatch} from 'react-redux';
import {authLogin} from '../../redux/features/authSlice';
const Login = ({navigation}) => {
  const dispatch = useDispatch();
  const [Email, setEmail] = useState('');
  const [Password, setPassword] = useState('');
  const keyboardHeight = useRef(new Animated.Value(0)).current;
  const fadeAnim = useRef(new Animated.Value(0)).current;

  useEffect(() => {
    Keyboard.addListener('keyboardDidShow', keyboardWillShow);
    Keyboard.addListener('keyboardDidHide', keyboardWillHide);
    Animated.timing(fadeAnim, {
      toValue: 1,
      duration: 300,
      useNativeDriver: true,
    }).start();
    // cleanup function
    return () => {
      Keyboard.removeListener('keyboardDidShow', keyboardWillShow);
      Keyboard.removeListener('keyboardDidHide', keyboardWillHide);
    };
  }, []);

  const keyboardWillShow = (event) => {
    console.log('keyboardWillShow', event);
    Animated.parallel([
      Animated.timing(keyboardHeight, {
        duration: event.duration,
        toValue: event.endCoordinates.height - 130,
        // useNativeDriver: true,
      }),
    ]).start();
  };

  const keyboardWillHide = (event) => {
    console.log('keyboardWillHide');
    Animated.parallel([
      Animated.timing(keyboardHeight, {
        duration: event.duration,
        toValue: 0,
      }),
    ]).start();
  };
  return (
    <Animated.View
      style={{
        flex: 1,
        opacity: fadeAnim,
      }}>
      <View
        style={{
          backgroundColor: 'purple',
          flex: 2.5,
        }}></View>
      <View
        style={{
          flex: 1,
          backgroundColor: 'white',
          flex: 2,
          alignItems: 'center',
          justifyContent: 'flex-end',
        }}>
        <TouchableOpacity onPress={() => navigation.navigate('Register')}>
          <Text
            style={{
              bottom: 10,
              fontFamily: 'OpenSansCondensed-Bold',
              fontSize: 15,
            }}>
            Don't have an account?
            <Text style={{color: 'purple'}}> Sign Up</Text>
          </Text>
        </TouchableOpacity>
      </View>
      <Animated.View
        style={{
          position: 'absolute',
          height: '100%',
          width: '100%',
          bottom: keyboardHeight,
          alignItems: 'center',
        }}>
        <Text
          style={{
            fontFamily: 'SansitaSwashed-Light',
            fontSize: 40,
            color: 'yellow',
            marginVertical: '35%',
          }}>
          DJAJAN
        </Text>

        <View
          style={{
            backgroundColor: 'white',
            paddingHorizontal: 20,
            paddingVertical: 10,
            minWidth: '70%',
            elevation: 5,
            borderRadius: 5,
          }}>
          <View style={{minWidth: '70%'}}>
            <Text
              style={{
                fontFamily: 'OpenSansCondensed-Light',
                fontSize: 40,
                color: 'black',
              }}>
              Sign In
            </Text>
            <Input
              placeholder="Email"
              value={Email}
              icons="envelope"
              onChange={(e) => setEmail(e)}
              width="70%"
            />
            <Input
              placeholder="Password"
              password
              icons="unlock-alt"
              value={Password}
              onChange={(e) => setPassword(e)}
              width="70%"
            />
          </View>
          <Button
            title="Login"
            onPress={() => dispatch(authLogin(Email, Password))}
          />
        </View>
      </Animated.View>
    </Animated.View>
  );
};

export default Login;
