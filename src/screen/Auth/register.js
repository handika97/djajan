import React, {useState, useEffect, useRef} from 'react';
import {
  View,
  Image,
  Text,
  TouchableOpacity,
  Animated,
  Keyboard,
} from 'react-native';
import {Input, Button} from '../../component/cell';
import {authRegister} from '../../redux/features/authSlice';
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';
import {useDispatch} from 'react-redux';

const Register = ({navigation}) => {
  const dispatch = useDispatch();
  const [Name, setName] = useState('');
  const [Email, setEmail] = useState('');
  const [Password, setPassword] = useState('');
  const keyboardHeight = useRef(new Animated.Value(0)).current;
  const fadeAnim = useRef(new Animated.Value(0)).current;

  useEffect(() => {
    Keyboard.addListener('keyboardDidShow', keyboardWillShow);
    Keyboard.addListener('keyboardDidHide', keyboardWillHide);
    Animated.timing(fadeAnim, {
      toValue: 1,
      duration: 300,
      useNativeDriver: true,
    }).start();
    // cleanup function
    return () => {
      Keyboard.removeListener('keyboardDidShow', keyboardWillShow);
      Keyboard.removeListener('keyboardDidHide', keyboardWillHide);
    };
  }, []);

  const keyboardWillShow = (event) => {
    console.log('keyboardWillShow', event);
    Animated.parallel([
      Animated.timing(keyboardHeight, {
        duration: event.duration,
        toValue: event.endCoordinates.height - 130,
        // useNativeDriver: true,
      }),
    ]).start();
  };

  const keyboardWillHide = (event) => {
    console.log('keyboardWillHide');
    Animated.parallel([
      Animated.timing(keyboardHeight, {
        duration: event.duration,
        toValue: 0,
        // useNativeDriver: true,
      }),
    ]).start();
  };
  return (
    <Animated.View
      style={{
        flex: 1,
        opacity: fadeAnim,
      }}>
      <View
        style={{
          backgroundColor: 'purple',
          flex: 2.5,
        }}></View>
      <View
        style={{
          flex: 1,
          backgroundColor: 'white',
          flex: 2,
          alignItems: 'center',
          justifyContent: 'flex-end',
        }}>
        <TouchableOpacity onPress={() => navigation.navigate('Login')}>
          <Text
            style={{
              bottom: 10,
              fontFamily: 'OpenSansCondensed-Bold',
              fontSize: 15,
            }}>
            Have an account
            <Text style={{color: 'purple'}}> Sign In</Text>
          </Text>
        </TouchableOpacity>
      </View>
      <Animated.View
        style={{
          position: 'absolute',
          height: '100%',
          width: '100%',
          bottom: keyboardHeight,
          alignItems: 'center',
        }}>
        <Text
          style={{
            fontFamily: 'SansitaSwashed-Light',
            fontSize: 40,
            color: 'yellow',
            marginTop: '35%',
            marginBottom: '20%',
          }}>
          DJAJAN
        </Text>

        <View
          style={{
            backgroundColor: 'white',
            paddingHorizontal: 20,
            paddingVertical: 10,
            minWidth: '70%',
            elevation: 5,
            borderRadius: 5,
          }}>
          <View style={{minWidth: '70%'}}>
            <Text
              style={{
                fontFamily: 'OpenSansCondensed-Light',
                fontSize: 40,
                color: 'black',
              }}>
              Sign Up
            </Text>
            <Input
              placeholder="Name"
              value={Name}
              icons="user"
              onChange={(e) => setName(e)}
            />
            {/* <Input
              placeholder="Phone"
              value={Email}
              icons="phone-alt"
              type="numeric"
              onChange={(e) => setEmail(e)}
            /> */}
            <Input
              placeholder="Email"
              value={Email}
              icons="envelope"
              onChange={(e) => setEmail(e)}
            />
            <Input
              placeholder="Password"
              password
              icons="unlock-alt"
              value={Password}
              onChange={(e) => setPassword(e)}
            />
          </View>
          <Button
            title="Registration"
            onPress={() => {
              dispatch(authRegister(Name, Email, Password));
            }}
          />
        </View>
      </Animated.View>
    </Animated.View>
  );
};

export default Register;
