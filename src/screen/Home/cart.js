import React, {useState, useEffect, useRef} from 'react';
import {
  View,
  Image,
  Text,
  TouchableOpacity,
  Animated,
  Keyboard,
  ScrollView,
  FlatList,
} from 'react-native';
import {Loading, Button} from '../../component/cell';
import {useDispatch, useSelector} from 'react-redux';
import {Plus, Empty, Minus} from '../../redux/features/cart';
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';

const DetailStore = ({navigation, route}) => {
  const [loading, setLoading] = useState(false);
  const cart = useSelector((state) => state.cart);
  const dispatch = useDispatch();
  console.log(cart.data.length);
  const plus = (item) => {
    dispatch(Plus(item, cart));
  };

  return (
    <View style={{flex: 1, backgroundColor: 'white'}}>
      {loading && (
        <View
          style={{flex: 1, justifyContent: 'center', alignContent: 'center'}}>
          <Loading text="Loading" />
        </View>
      )}
      {!loading && cart.data?.length > 0 && (
        <View style={{width: '100%'}}>
          <ScrollView style={{padding: 2}}>
            <View style={{paddingHorizontal: 15, marginVertical: 10}}>
              <Text
                style={{
                  fontWeight: 'bold',
                  fontSize: 17,
                  marginVertical: 10,
                  alignSelf: 'center',
                }}>
                Keranjang Belanja
              </Text>
              <FlatList
                data={cart.data}
                renderItem={({item}) => {
                  return (
                    <View
                      style={{
                        flexDirection: 'row',
                        justifyContent: 'space-between',
                        marginVertical: 5,
                        borderBottomWidth: 0.5,
                        paddingBottom: 10,
                      }}>
                      <View
                        style={{
                          flex: 0.7,
                        }}>
                        <Text
                          style={{
                            // fontWeight: 'bold',
                            fontSize: 17,
                            fontFamily: 'OpenSansCondensed-Bold',
                          }}>
                          {item.item.Title}
                        </Text>
                        <Text
                          style={{
                            fontFamily: 'OpenSans-Regular',
                          }}>
                          {item.item.Description}
                        </Text>
                      </View>
                      <View style={{flex: 0.25}}>
                        <Text
                          style={{
                            fontWeight: '900',
                            fontSize: 15,
                          }}>
                          Rp. {item.item.Price}
                        </Text>
                        <View
                          style={{
                            flexDirection: 'row',
                            justifyContent: 'space-between',
                            alignItems: 'center',
                            marginTop: 10,
                          }}>
                          <TouchableOpacity
                            style={{
                              backgroundColor: 'purple',
                              height: 25,
                              width: 25,
                              justifyContent: 'center',
                              alignItems: 'center',
                              borderRadius: 20,
                            }}
                            onPress={() => {
                              dispatch(Plus(item.item, cart));
                            }}>
                            <FontAwesome5 name="plus" size={20} color="white" />
                          </TouchableOpacity>
                          <Text>{item.qty}</Text>
                          <TouchableOpacity
                            style={{
                              backgroundColor: 'purple',
                              height: 25,
                              width: 25,
                              justifyContent: 'center',
                              alignItems: 'center',
                              borderRadius: 20,
                            }}
                            onPress={() => dispatch(Minus(item.item, cart))}>
                            <FontAwesome5
                              name="minus"
                              size={20}
                              color="white"
                            />
                          </TouchableOpacity>
                        </View>
                      </View>
                    </View>
                  );
                }}
              />
            </View>
          </ScrollView>
        </View>
      )}
      {cart.data?.length === 0 && (
        <View style={{flex: 1, height: 300, width: '100%'}}>
          <Image
            source={require('../../assets/images/icon_keranjang_kosong.png')}
            style={{height: '100%', width: '100%', resizeMode: 'contain'}}
          />
        </View>
      )}
    </View>
  );
};

export default DetailStore;
