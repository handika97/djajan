import React, {useState, useEffect, useRef} from 'react';
import {
  View,
  Image,
  Text,
  StyleSheet,
  TouchableOpacity,
  Keyboard,
  PermissionsAndroid,
  ActivityIndicator,
  Animated,
} from 'react-native';
import MapView, {PROVIDER_GOOGLE} from 'react-native-maps';
import {Input, Button, Loading} from '../../component/cell';
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';
import Geolocation from 'react-native-geolocation-service';
import {useDispatch} from 'react-redux';
import {authLogOut} from '../../redux/features/authSlice';
const Location = ({navigation}) => {
  const [location, setLocation] = useState({});
  const Margin = useRef(new Animated.Value(0)).current;

  const dispatch = useDispatch();
  useEffect(() => {
    Permision();
  }, []);

  const Permision = async () => {
    try {
      const granted = await PermissionsAndroid.request(
        PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION,
        {
          title: 'Izinkan Aplikasi Mengakses Lokasi Anda',
          buttonNeutral: 'Ask Me Later',
          buttonNegative: 'Cancel',
          buttonPositive: 'OK',
        },
        PermissionsAndroid.PERMISSIONS.ACCESS_COARSE_LOCATION,
        {
          title: 'Izinkan Aplikasi Mengakses Lokasi Anda',
          buttonNeutral: 'Ask Me Later',
          buttonNegative: 'Cancel',
          buttonPositive: 'OK',
        },
      );
      if (granted === PermissionsAndroid.RESULTS.GRANTED) {
        Geolocation.getCurrentPosition(
          (position) => {
            console.log(position.coords.latitude);
            setLocation({
              latitude: position.coords.latitude,
              longitude: position.coords.longitude,
              latitudeDelta: 0.008253919629830797,
              longitudeDelta: 0.004210062325014974,
            });
          },
          (error) => {
            // See error code charts below.
            console.log(error.code, error.message);
          },
          {enableHighAccuracy: true, timeout: 15000, maximumAge: 10000},
        );
      } else {
        alert('Aplikasi Membutuhkan Lokasi Anda');
      }
    } catch (err) {
      console.warn(err);
    }
  };
  console.log(location);

  useEffect(() => {
    runAnimated();
  });
  const runAnimated = () => {
    Animated.loop(
      Animated.timing(Margin, {
        toValue: -10,
        duration: 500,
        useNativeDriver: true,
      }),
    ).start();
  };

  return (
    <View style={styles.container}>
      {location.latitude ? (
        <View style={styles.container}>
          <MapView
            provider={PROVIDER_GOOGLE} // remove if not using Google Maps
            style={styles.map}
            onRegionChange={(e) => console.log('change', e)}
            region={location}>
            {/* {location.latitude && <Marker coordinate={location} />} */}
          </MapView>
          <View
            style={{
              height: '100%',
              width: '100%',
              justifyContent: 'center',
              alignItems: 'center',
              position: 'absolute',
            }}>
            <Animated.View style={{transform: [{translateY: Margin}]}}>
              <FontAwesome5
                name="map-marker-alt"
                size={35}
                color="purple"
                style={{bottom: 10}}
              />
            </Animated.View>
          </View>
          <View
            style={{
              height: '100%',
              width: '100%',
              justifyContent: 'flex-end',
              alignItems: 'center',
              position: 'absolute',
            }}>
            <View style={{width: '50%', bottom: '10%'}}>
              <Button
                title="Pilih Lokasi"
                onPress={() => navigation.navigate('ListLocations')}
              />
            </View>
          </View>
        </View>
      ) : (
        <View style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
          <Loading text="Sedang Mencari Lokasi Anda" />
        </View>
      )}
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    ...StyleSheet.absoluteFillObject,
    height: '100%',
    width: '100%',
    justifyContent: 'flex-end',
    alignItems: 'center',
  },
  map: {
    ...StyleSheet.absoluteFillObject,
  },
});

export default Location;
