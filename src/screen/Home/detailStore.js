import React, {useState, useEffect, useRef} from 'react';
import {
  View,
  Image,
  Text,
  TouchableOpacity,
  Animated,
  Keyboard,
  ScrollView,
  FlatList,
} from 'react-native';
import {Loading, Button} from '../../component/cell';
import {useDispatch, useSelector} from 'react-redux';
import {Plus, Reset, Minus} from '../../redux/features/cart';
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';

const albums = [
  {
    Title: 'Manis',
    id: 1,
    Description: 'dengan rasa manis',
    Price: 10000,
  },
  {
    Title: 'Pedas',
    id: 2,
    Description: 'dengan rasa Pedas',
    Price: 5000,
  },
  {
    Title: 'Asem',
    id: 3,
    Description: 'dengan rasa Asem',
    Price: 11000,
  },
  {
    Title: 'Pahit',
    id: 4,
    Description: 'dengan rasa Pahit',
    Price: 15000,
  },
];

const List = (item, navigation) => {
  return (
    <TouchableOpacity
      style={{
        minHeight: 200,
        width: '95%',
        borderRadius: 5,
        elevation: 2,
        backgroundColor: 'white',
        marginVertical: 10,
        marginHorizontal: '1%',
        paddingBottom: 1,
      }}
      onPress={() => {
        navigation.navigate('Home', {screen: 'Location'});
      }}>
      <View
        style={{
          height: 150,
        }}>
        <Image
          source={require('../../assets/images/example.jpg')}
          style={{
            height: '100%',
            width: '100%',
            borderTopLeftRadius: 5,
            borderTopRightRadius: 5,
          }}
        />
      </View>
      <Text
        style={{
          fontFamily: 'BarlowCondensed-SemiBold',
          marginLeft: 2,
        }}>
        {item.loc}
      </Text>
      <Text
        style={{
          fontWeight: '100',
          fontSize: 12,
          marginVertical: 4,
        }}>
        100 Km Dari Posisi Anda
      </Text>
    </TouchableOpacity>
  );
};

const CartList = ({navigation, route}) => {
  const [loading, setLoading] = useState(false);
  const cart = useSelector((state) => state.cart);
  const {item} = route.params;
  const dispatch = useDispatch();

  const plus = (item) => {
    dispatch(Plus(item, cart));
  };

  useEffect(() => {
    // dispatch(Reset());
  }, []);

  return (
    <View style={{flex: 1, backgroundColor: 'white'}}>
      {loading && (
        <View
          style={{flex: 1, justifyContent: 'center', alignContent: 'center'}}>
          <Loading text="Loading" />
        </View>
      )}
      {!loading && (
        <View style={{width: '100%'}}>
          <ScrollView style={{padding: 2, marginBottom: 70}}>
            <View
              style={{
                height: 170,
              }}>
              <Image
                source={require('../../assets/images/example.jpg')}
                style={{
                  height: '100%',
                  width: '100%',
                }}
              />
            </View>
            <View
              style={{
                flexDirection: 'row',
                justifyContent: 'space-between',
                padding: 10,
              }}>
              <Text
                style={{
                  fontSize: 20,
                  fontFamily: 'BarlowCondensed-SemiBoldItalic',
                  flex: 0.7,
                }}>
                {item.name}
              </Text>
              <Text
                style={{
                  fontSize: 20,
                  fontFamily: 'BarlowCondensed-SemiBoldItalic',
                  flex: 0.2,
                }}>
                {item.RatePrice}
              </Text>
            </View>
            <View style={{paddingHorizontal: 15, marginVertical: 10}}>
              <Text
                style={{
                  fontWeight: 'bold',
                  fontSize: 17,
                  marginVertical: 10,
                  alignSelf: 'center',
                }}>
                Daftar Menu
              </Text>
              <FlatList
                data={albums}
                renderItem={({item}) => {
                  return (
                    <View
                      style={{
                        flexDirection: 'row',
                        justifyContent: 'space-between',
                        marginVertical: 5,
                        borderBottomWidth: 0.5,
                        paddingBottom: 10,
                      }}>
                      <View
                        style={{
                          flex: 0.7,
                        }}>
                        <Text
                          style={{
                            // fontWeight: 'bold',
                            fontSize: 17,
                            fontFamily: 'OpenSansCondensed-Bold',
                          }}>
                          {item.Title}
                        </Text>
                        <Text style={{fontFamily: 'OpenSans-Regular'}}>
                          {item.Description}
                        </Text>
                      </View>
                      <View style={{flex: 0.25}}>
                        <Text
                          style={{
                            fontWeight: '900',
                            fontSize: 15,
                          }}>
                          Rp. {item.Price}
                        </Text>
                        <View
                          style={{
                            flexDirection: 'row',
                            justifyContent: 'space-between',
                            alignItems: 'center',
                            marginTop: 10,
                          }}>
                          {cart.data.length > 0 ? (
                            cart.data
                              .map(function (obj) {
                                return obj.id;
                              })
                              .indexOf(item.id) === -1 ? (
                              <View style={{height: 25, width: 70}}>
                                <Button
                                  title="Tambah"
                                  onPress={() => plus(item)}
                                  small
                                />
                              </View>
                            ) : (
                              <>
                                <TouchableOpacity
                                  style={{
                                    backgroundColor: 'purple',
                                    height: 25,
                                    width: 25,
                                    justifyContent: 'center',
                                    alignItems: 'center',
                                    borderRadius: 20,
                                  }}
                                  onPress={() => {
                                    plus(item);
                                  }}>
                                  <FontAwesome5
                                    name="plus"
                                    size={20}
                                    color="white"
                                  />
                                </TouchableOpacity>
                                <Text>
                                  {cart.data.length > 0 &&
                                  cart.data
                                    .map(function (obj) {
                                      return obj.id;
                                    })
                                    .indexOf(item.id) === -1
                                    ? '0'
                                    : cart.data[
                                        cart.data
                                          .map((x) => {
                                            return x.id;
                                          })
                                          .indexOf(item.id)
                                      ].qty}
                                </Text>
                                <TouchableOpacity
                                  style={{
                                    backgroundColor: 'purple',
                                    height: 25,
                                    width: 25,
                                    justifyContent: 'center',
                                    alignItems: 'center',
                                    borderRadius: 20,
                                  }}
                                  onPress={() => dispatch(Minus(item, cart))}>
                                  <FontAwesome5
                                    name="minus"
                                    size={20}
                                    color="white"
                                  />
                                </TouchableOpacity>
                              </>
                            )
                          ) : (
                            <View style={{height: 25, width: 70}}>
                              <Button
                                title="Tambah"
                                onPress={() => plus(item)}
                                small
                              />
                            </View>
                          )}
                        </View>
                      </View>
                    </View>
                  );
                }}
              />
            </View>
          </ScrollView>
        </View>
      )}
    </View>
  );
};

export default CartList;
