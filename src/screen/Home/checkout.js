import React, {useState, useEffect, useRef} from 'react';
import {
  View,
  Image,
  Text,
  TouchableOpacity,
  Animated,
  Keyboard,
  ScrollView,
  FlatList,
} from 'react-native';
import LinearGradient from 'react-native-linear-gradient';
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';
import {Loading, Input, Button} from '../../component/cell';
import {useDispatch, useSelector} from 'react-redux';
import {AddStore, Reset, Empty} from '../../redux/features/cart';
const albums = [
  {
    loc:
      'Jl. Teuku Umar No.1, RT.1/RW.1, Gondangdia, Kec. Menteng, Kota Jakarta Pusat, Daerah Khusus Ibukota Jakarta 10350',
    Jarak: 100,
    id: 1,
  },
  {
    loc:
      'Jl. Samanhudi No.65, RT.8/RW.6, Ps. Baru, Kecamatan Sawah Besar, Kota Jakarta Pusat, Daerah Khusus Ibukota Jakarta 10710',
    Jarak: 100,
    id: 2,
  },
  {
    loc:
      'Wahid Hasyim Street No.70, RT.7/RW.5, Kebon Sirih, Menteng, Central Jakarta City, Jakarta 10340',
    Jarak: 100,
    id: 3,
  },
  {
    loc:
      'Jl. Cisadane No.121, RT.9/RW.4, Cikini, Kec. Menteng, Kota Jakarta Pusat, Daerah Khusus Ibukota Jakarta 10330',
    Jarak: 100,
    id: 4,
  },
  {
    loc:
      'Wahid Hasyim Street No.70, RT.7/RW.5, Kebon Sirih, Menteng, Central Jakarta City, Jakarta 10340',
    Jarak: 100,
    id: 3,
  },
  {
    loc:
      'Jl. Cisadane No.121, RT.9/RW.4, Cikini, Kec. Menteng, Kota Jakarta Pusat, Daerah Khusus Ibukota Jakarta 10330',
    Jarak: 100,
    id: 4,
  },
  {
    loc:
      'Wahid Hasyim Street No.70, RT.7/RW.5, Kebon Sirih, Menteng, Central Jakarta City, Jakarta 10340',
    Jarak: 100,
    id: 3,
  },
  {
    loc:
      'Jl. Cisadane No.121, RT.9/RW.4, Cikini, Kec. Menteng, Kota Jakarta Pusat, Daerah Khusus Ibukota Jakarta 10330',
    Jarak: 100,
    id: 4,
  },
  {
    loc:
      'Wahid Hasyim Street No.70, RT.7/RW.5, Kebon Sirih, Menteng, Central Jakarta City, Jakarta 10340',
    Jarak: 100,
    id: 3,
  },
  {
    loc:
      'Jl. Cisadane No.121, RT.9/RW.4, Cikini, Kec. Menteng, Kota Jakarta Pusat, Daerah Khusus Ibukota Jakarta 10330',
    Jarak: 100,
    id: 4,
  },
  {
    loc:
      'Wahid Hasyim Street No.70, RT.7/RW.5, Kebon Sirih, Menteng, Central Jakarta City, Jakarta 10340',
    Jarak: 100,
    id: 3,
  },
  {
    loc:
      'Jl. Cisadane No.121, RT.9/RW.4, Cikini, Kec. Menteng, Kota Jakarta Pusat, Daerah Khusus Ibukota Jakarta 10330',
    Jarak: 100,
    id: 4,
  },
  {
    loc:
      'Wahid Hasyim Street No.70, RT.7/RW.5, Kebon Sirih, Menteng, Central Jakarta City, Jakarta 10340',
    Jarak: 100,
    id: 3,
  },
  {
    loc:
      'Jl. Cisadane No.121, RT.9/RW.4, Cikini, Kec. Menteng, Kota Jakarta Pusat, Daerah Khusus Ibukota Jakarta 10330',
    Jarak: 100,
    id: 4,
  },
  {
    loc:
      'Wahid Hasyim Street No.70, RT.7/RW.5, Kebon Sirih, Menteng, Central Jakarta City, Jakarta 10340',
    Jarak: 100,
    id: 3,
  },
  {
    loc:
      'Jl. Cisadane No.121, RT.9/RW.4, Cikini, Kec. Menteng, Kota Jakarta Pusat, Daerah Khusus Ibukota Jakarta 10330',
    Jarak: 100,
    id: 4,
  },
  {
    loc:
      'Wahid Hasyim Street No.70, RT.7/RW.5, Kebon Sirih, Menteng, Central Jakarta City, Jakarta 10340',
    Jarak: 100,
    id: 3,
  },
  {
    loc:
      'Jl. Cisadane No.121, RT.9/RW.4, Cikini, Kec. Menteng, Kota Jakarta Pusat, Daerah Khusus Ibukota Jakarta 10330',
    Jarak: 100,
    id: 4,
  },
];

const List = (item, navigation, store, dispatch = () => {}, ResetStore) => {
  return (
    <View
      style={{
        justifyContent: 'center',
        alignItems: 'center',
        padding: 4,
      }}>
      <TouchableOpacity
        style={{
          minHeight: 200,
          width: '100%',
          borderRadius: 5,
          elevation: 2,
          backgroundColor: 'white',
          marginVertical: 5,
          paddingBottom: 1,
        }}
        onPress={() => {
          !store || store === item.id
            ? (navigation.navigate('store', {
                screen: 'Warung',
                params: {id: item.id},
              }),
              dispatch(AddStore(item.id)),
              console.log(store, item.id))
            : ResetStore(item.id);
        }}>
        <View
          style={{
            height: 150,
          }}>
          <Image
            source={require('../../assets/images/example.jpg')}
            style={{
              height: '100%',
              width: '100%',
              borderTopLeftRadius: 5,
              borderTopRightRadius: 5,
            }}
          />
        </View>
        <View style={{padding: 2}}>
          <Text
            style={{
              fontFamily: 'BarlowCondensed-SemiBold',
              marginLeft: 2,
            }}>
            {item.loc}
          </Text>
          <Text
            style={{
              fontWeight: '100',
              fontSize: 12,
              marginVertical: 4,
            }}>
            100 Km Dari Posisi Anda
          </Text>
        </View>
      </TouchableOpacity>
    </View>
  );
};
const ListLocations = ({navigation}) => {
  const [loading, setLoading] = useState(true);
  const TranslateY = useRef(new Animated.Value(550)).current;
  const TranslateYModal = useRef(new Animated.Value(200)).current;
  const TranslateScrollY = useRef(new Animated.Value(0)).current;
  const [search, setSearch] = useState('');
  const [listItem, setListItem] = useState(albums);
  const [ResetStore, setResetStore] = useState(false);
  const [idStore, setidStore] = useState('');
  const [ScrollY, setScrollY] = useState(0);
  const store = useSelector((state) => state.cart.store);
  const dispatch = useDispatch();
  useEffect(() => {
    if (loading) {
      Animated.timing(TranslateY, {
        toValue: 550,
        duration: 500,
        useNativeDriver: true,
      }).start();
    } else {
      Animated.timing(TranslateY, {
        toValue: 0,
        duration: 500,
        useNativeDriver: true,
      }).start();
    }
  }, [loading]);

  useEffect(() => {
    setTimeout(() => {
      setLoading(!loading);
    }, 3000);
  }, []);

  useEffect(() => {
    setListItem(
      search
        ? albums.filter((item) => {
            return item.loc.toLowerCase().indexOf(search.toLowerCase()) > -1;
          })
        : albums,
    );
  }, [search]);

  useEffect(() => {
    if (ResetStore) {
      Animated.timing(TranslateYModal, {
        toValue: 0,
        duration: 500,
        useNativeDriver: true,
      }).start();
      Animated.timing(TranslateScrollY, {
        toValue: 70,
        duration: 200,
        useNativeDriver: true,
      }).start();
    } else {
      Animated.timing(TranslateYModal, {
        toValue: 200,
        duration: 500,
        useNativeDriver: true,
      }).start();
      Animated.timing(TranslateScrollY, {
        toValue: 0,
        duration: 200,
        useNativeDriver: true,
      }).start();
    }
  }, [ResetStore]);

  const handleScroll = (event) => {
    // console.log(event.nativeEvent.contentOffset.y);
    setScrollY(event.nativeEvent.contentOffset.y);
    if (event.nativeEvent.contentOffset.y > ScrollY) {
      Animated.timing(TranslateScrollY, {
        toValue: 70,
        duration: 200,
        useNativeDriver: true,
      }).start();
    } else {
      Animated.timing(TranslateScrollY, {
        toValue: 0,
        duration: 200,
        useNativeDriver: true,
      }).start();
    }
  };

  return (
    <View
      style={{
        flex: 1,
      }}>
      <LinearGradient
        start={{x: 0.0, y: 0.0}}
        end={{x: 1, y: 1.0}}
        locations={[0, 0.4, 0.7]}
        colors={['purple', '#db4dff', '#e580ff']}
        style={{
          flex: 1,
          paddingHorizontal: '3%',
          paddingTop: '5%',
          zIndex: 1,
        }}>
        <View
          style={{
            backgroundColor: 'white',
            flex: 1,
            borderTopLeftRadius: 10,
            borderTopRightRadius: 10,
            elevation: 5,
            paddingVertical: 17,
            paddingHorizontal: '2%',
          }}></View>
      </LinearGradient>
    </View>
  );
};

export default ListLocations;
