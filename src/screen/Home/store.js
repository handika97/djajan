import React, {useState, useEffect, useRef} from 'react';
import {
  View,
  Image,
  Text,
  TouchableOpacity,
  Animated,
  Keyboard,
  ScrollView,
  FlatList,
} from 'react-native';
import {Loading, Button, Input} from '../../component/cell';
import {useDispatch, useSelector} from 'react-redux';
import {AddStore} from '../../redux/features/cart';

const albums = [
  {
    name: 'Telur Gulung',
    RatePrice: '$$',
  },
  {
    name: 'Martabak Manis Pak Sutopo',
    RatePrice: '$$',
  },
  {
    name: 'Cilung',
    RatePrice: '$$',
  },
  {
    name: 'Fried Chicken',
    RatePrice: '$$',
  },
];

const List = (item, navigation) => {
  return (
    <View
      style={{
        alignItems: 'center',
      }}>
      <TouchableOpacity
        style={{
          // minHeight: 100,
          maxHeight: 110,
          width: '95%',
          borderRadius: 5,
          backgroundColor: 'white',
          marginVertical: 5,
          flexDirection: 'row',
          justifyContent: 'space-between',
          elevation: 5,
        }}
        onPress={() => {
          navigation.navigate('DetailStore', {item: item});
        }}>
        <View
          style={{
            flex: 0.5,
          }}>
          <Image
            source={require('../../assets/images/example.jpg')}
            style={{
              height: '100%',
              width: '100%',
              resizeMode: 'cover',
              borderTopLeftRadius: 5,
              borderBottomLeftRadius: 5,
            }}
          />
        </View>
        <View
          style={{
            flex: 0.5,
            backgroundColor: '#f2f2f2',
            borderToprightRadius: 5,
            borderBottomrightRadius: 5,
          }}>
          <View style={{margin: 3}}>
            <Text
              style={{
                fontFamily: 'OpenSansCondensed-Bold',
                fontSize: 15,
                // marginLeft: 2,
              }}>
              {item.name}
            </Text>
            <Text
              style={{
                fontWeight: '100',
                fontSize: 12,
                marginVertical: 4,
                fontFamily: 'OpenSans-Regular',
              }}>
              {item.RatePrice}
            </Text>
            <Text
              style={{
                fontWeight: '100',
                fontSize: 12,
                marginVertical: 4,
                fontFamily: 'OpenSans-Regular',
              }}>
              Jam Buka: 07.00 - 22.00
            </Text>
          </View>
        </View>
      </TouchableOpacity>
    </View>
  );
};

const StoreList = ({navigation, route}) => {
  const [loading, setLoading] = useState(false);
  const [search, setSearch] = useState('');

  // useEffect(() => {
  //   setTimeout(() => {
  //     setLoading(!loading);
  //   }, 3000);
  // }, []);

  return (
    <View style={{flex: 1, backgroundColor: 'white'}}>
      {loading && (
        <View
          style={{flex: 1, justifyContent: 'center', alignContent: 'center'}}>
          <Loading text="Loading" />
        </View>
      )}
      {!loading && (
        <View style={{width: '100%'}}>
          <View
            style={{
              borderBottomWidth: 0.5,
              borderBottomColor: 'grey',
              margin: 10,
              paddingBottom: 5,
              flexDirection: 'row',
              justifyContent: 'space-between',
              alignItems: 'center',
            }}>
            <View
              style={{
                flex: 0.4,
              }}>
              <Text
                style={{
                  fontSize: 18,
                  fontFamily: 'OpenSans-SemiBold',
                }}>
                Pilih Warung
              </Text>
            </View>
            <View style={{flex: 0.4}}>
              <Input
                placeholder="Cari Warung"
                icons="search"
                noBorder
                value={search}
                onChange={(e) => setSearch(e)}
              />
            </View>
          </View>
          <ScrollView>
            <FlatList
              data={
                search
                  ? albums.filter((item) => {
                      return (
                        item.name.toLowerCase().indexOf(search.toLowerCase()) >
                        -1
                      );
                    })
                  : albums
              }
              renderItem={({item}) => List(item, navigation)}
              keyExtractor={(item) => item.id}
              style={{}}
            />
            {albums.filter((item) => {
              return item.name.toLowerCase().indexOf(search.toLowerCase()) > -1;
            }).length === 0 && (
              <View
                style={{
                  flex: 1,
                  judtifyContent: 'center',
                  alignItems: 'center',
                  height: '100%',
                  marginVertical: 100,
                }}>
                <Image
                  source={require('../../assets/images/searchNotFound.png')}
                />
              </View>
            )}
          </ScrollView>
        </View>
      )}
    </View>
  );
};

export default StoreList;
